# Cheatsheet sobre o Git

## Sobre

Conteúdo criado durante o treinamento "Descomplicando o Gitlab"

Auxílio para os alunos e todos que estão na jornada para aprender a dominar o Git.

## Conteúdo

### Git Config


##### Adicionar nome:

```bash
git config --global user.name "<user_name>"
```

##### Adicionar e-mail

```bash
git config --global user.email "<user@user.com>"
```

##### Configurar editor de texto padrão

```bash
git config --system core.editor "<core_editor>"
```

##### Visualizar e editar configurações
bash
```
git config --global --edit
``` 

##### Checando o arquivo de configuração do Git no diretório home do usuário corrente

```bash
sudo nano $HOME/.gitconfig
```


##### Criando alias 

###### commit

```bash
git config --global alias.c "commit -am"
``` 

##### Listando configurações na CLI

```bash
git config --list
git config --system --list 
git config --global --list
```


##### Adicionando cores a ui

```bash
sudo git config --system color.ui true
```

## Comandos Básicos 

##### Iniciando o repositório

```bash
git init
```

##### Checando o status do repositório

```bash
git status
``` 

##### Adicionando arquivos ao repositório

```bash
git add . # Adicionando todos os arquivos
git add <file> # adicionando um único arquivo
git add <file1> <file2> <file3> # adicionando multiplos arquivos
git add <folder>/ # adicionando tudo que estiver dentro de um diretório
```

##### Enviando alterações para o servidor remoto

```bash
git push # enviando da branch atual
git push origin/<branch> # enviando de uma branch específica
```

##### Checando os logs das operações

```bash
git log 
git log -1 # Checa apenas o ultimo log
git log --oneline -1 # Traz a informação de forma condensada
git log -p # Traz os logs em forma de mensagem, com maiores detalhes
git log --pretty=? # Formas diferentes de apresentação
git log -- <file> # Mostra se existe algum log relativo ao arquivo
git log --graph --decorate # Mostra os logs em forma de grafico
git log --author=<name> # Mostra os logs por autor
git log --stat # mostra o que foi alterado no commit
```


## Branches 

##### Renomeando a branch master para main

```bash
git branch -m master main
git push -u origin/main
git symbolic-ref refs/remotes/origin/HEAD refs/remotes/origin/main
git branch -a 
``` 

##### Criando uma branch

```bash
git checkout -b <branch_name>
```

##### Navegando entre branches

```bash
git checkout <branch_name>
```

##### Verificando em qual branch estamos

```bash
git branch
```

##### Fazendo um push de uma branch, que criará uma merge request

```bash
git push origin <branch_name>
```

##### Efetuando o merge da branch para a main

```bash
git merge <branch_name>
```

##### Deletando uma branch localmente

```bash
git checkout -d <branch_name>
```

##### Deletando uma branch remotamente

```bash
git push -d origin <branch_name>
```


##### Reset em uma operação (Por exemplo, adicionar um arquivo indevido)


```bash
git restore --staged <file_name>
``` 

##### Reset em uma operação, apontando para o ultimo commit feito - PERIGO


```bash
git reset --hard 
ou
git reset <file_name>
ou
git reset <commit_hash>
``` 

##### Alterando a mensagem de um commit

```bash
git commit --amend
```


##### Alterando o apontamento do HEAD para um commit específico (Para isso o commit deve ter sido feito com a opção -am)

``` bash
git log # Para pegar a hash do commit para onde deseja apontar o HEAD
git revert <commit_hash>
```

##### Removendo um arquivo da workarea

```bash
git clean -n # Para mostrar 
git clean -f # Para remover
```

##### Checando diferenças entre os repositórios


```bash
git diff HEAD # Compara a diferença entre o Work Directory e o HEAD
git diff --cached # Compara o que está em staged na Work Directory e o Repo
```



